package ru.vmaksimenkov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.vmaksimenkov.tm.model.AbstractEntity;

public interface AbstractRepository<E extends AbstractEntity> extends JpaRepository<E, String> {

}