package ru.vmaksimenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.vmaksimenkov.tm.model.User;

@Repository
public interface UserRepository extends AbstractRepository<User> {

    @Nullable User findByLogin(@NotNull @Param("login") String login);

    @Query("SELECT id FROM User WHERE login = :login")
    @Nullable String getIdByLogin(@NotNull @Param("login") String login);

}
