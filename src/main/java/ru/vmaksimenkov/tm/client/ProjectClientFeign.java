package ru.vmaksimenkov.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.vmaksimenkov.tm.dto.ProjectRecord;

import java.util.Collection;
import java.util.List;

@FeignClient("project")
public interface ProjectClientFeign {

    static ProjectClientFeign client(@NotNull final String url) {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectClientFeign.class, url);
    }

    @Nullable
    @GetMapping("/{id}")
    ProjectRecord get(@NotNull @PathVariable("id") final String id);

    @PostMapping
    void post(@NotNull @RequestBody final ProjectRecord project);

    @PutMapping
    void put(@NotNull @RequestBody final ProjectRecord project);

    @DeleteMapping("/{id}")
    void delete(@NotNull @PathVariable("id") final String id);

    @NotNull
    @GetMapping
    Collection<ProjectRecord> get();

    @PostMapping
    void post(@NotNull @RequestBody final List<ProjectRecord> projects);

    @PutMapping
    void put(@NotNull @RequestBody final List<ProjectRecord> projects);

    @DeleteMapping
    void delete();

}
