package ru.vmaksimenkov.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.vmaksimenkov.tm.dto.TaskRecord;

import java.util.Collection;
import java.util.List;

@FeignClient("task")
public interface TaskClientFeign {

    static TaskClientFeign client(@NotNull final String url) {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskClientFeign.class, url);
    }

    @Nullable
    @GetMapping("/{id}")
    TaskRecord get(@NotNull @PathVariable("id") final String id);

    @PostMapping
    void post(@NotNull @RequestBody final TaskRecord task);

    @PutMapping
    void put(@NotNull @RequestBody final TaskRecord task);

    @DeleteMapping("/{id}")
    void delete(@NotNull @PathVariable("id") final String id);

    @NotNull
    @GetMapping
    Collection<TaskRecord> get();

    @PostMapping
    void post(@NotNull @RequestBody final List<TaskRecord> tasks);

    @PutMapping
    void put(@NotNull @RequestBody final List<TaskRecord> tasks);

    @DeleteMapping
    void delete();

}
