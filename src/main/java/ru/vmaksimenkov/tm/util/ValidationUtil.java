package ru.vmaksimenkov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.enumerated.RoleType;
import ru.vmaksimenkov.tm.enumerated.Sort;
import ru.vmaksimenkov.tm.enumerated.Status;

public interface ValidationUtil {

    static boolean checkIndex(final int index, final int size) {
        if (index < 1) return false;
        return index <= size;
    }

    static boolean checkIndex(final int index, final Long size) {
        if (index < 1) return false;
        return index <= size;
    }

    static boolean checkRole(@Nullable final String role) {
        for (@NotNull final RoleType s : RoleType.values()) {
            if (s.name().equals(role)) {
                return true;
            }
        }
        return false;
    }

    static boolean checkSort(@Nullable final String sortOrder) {
        for (@NotNull final Sort s : Sort.values()) {
            if (s.name().equals(sortOrder)) {
                return true;
            }
        }
        return false;
    }

    static boolean checkStatus(@Nullable final String status) {
        for (@NotNull final Status s : Status.values()) {
            if (s.name().equals(status)) {
                return true;
            }
        }
        return false;
    }

    static boolean isEmpty(@Nullable final String value) {
        return value == null || value.isEmpty();
    }

}
