package ru.vmaksimenkov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.vmaksimenkov.tm.AbstractTest;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ProjectCollectionEndpointTest extends AbstractTest {

    @Test
    @SneakyThrows
    public void post() {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/projects")
                        .content(asJsonString(PROJECT_LIST))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        get()
                .andExpect(jsonPath("$[*].id").value(org.hamcrest.Matchers.containsInAnyOrder(PROJECT.getId(), PROJECT2.getId())));
    }

    @Test
    @SneakyThrows
    public void put() {
        post();
        PROJECT_LIST.get(0).setName("Project Test Put");
        PROJECT_LIST.get(1).setName("Project Tested Put");
        mockMvc.perform(
                MockMvcRequestBuilders.put("/api/projects")
                        .content(asJsonString(PROJECT_LIST))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        get()
                .andExpect(jsonPath("$[*].name").value(org.hamcrest.Matchers.containsInAnyOrder("Project Test Put", "Project Tested Put")));
    }

    @Test
    @SneakyThrows
    public void delete() {
        post();
        mockMvc.perform(
                MockMvcRequestBuilders.delete("/api/projects")
                        .content(asJsonString(PROJECT_LIST))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        get()
                .andExpect(content().string("[]"));
    }

    @NotNull
    @SneakyThrows
    private ResultActions get() {
        return mockMvc.perform(
                MockMvcRequestBuilders.get("/api/projects")
                        .content(asJsonString(PROJECT))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}
