package ru.vmaksimenkov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.vmaksimenkov.tm.AbstractTest;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class TaskCollectionEndpointTest extends AbstractTest {

    @Test
    @SneakyThrows
    public void post() {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/tasks")
                        .content(asJsonString(TASK_LIST))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        get()
                .andExpect(jsonPath("$[*].id").value(org.hamcrest.Matchers.containsInAnyOrder(TASK.getId(), TASK2.getId())));
    }

    @Test
    @SneakyThrows
    public void put() {
        post();
        TASK_LIST.get(0).setName("Task Test Put");
        TASK_LIST.get(1).setName("Task Tested Put");
        mockMvc.perform(
                MockMvcRequestBuilders.put("/api/tasks")
                        .content(asJsonString(TASK_LIST))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        get()
                .andExpect(jsonPath("$[*].name").value(org.hamcrest.Matchers.containsInAnyOrder("Task Test Put", "Task Tested Put")));
    }

    @Test
    @SneakyThrows
    public void delete() {
        post();
        mockMvc.perform(
                MockMvcRequestBuilders.delete("/api/tasks")
                        .content(asJsonString(TASK_LIST))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        get()
                .andExpect(content().string("[]"));
    }

    @NotNull
    @SneakyThrows
    private ResultActions get() {
        return mockMvc.perform(
                MockMvcRequestBuilders.get("/api/tasks")
                        .content(asJsonString(TASK))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}
