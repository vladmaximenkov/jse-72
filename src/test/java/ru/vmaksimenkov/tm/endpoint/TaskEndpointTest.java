package ru.vmaksimenkov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.vmaksimenkov.tm.AbstractTest;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class TaskEndpointTest extends AbstractTest {

    @Test
    @SneakyThrows
    public void post() {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/task")
                        .content(asJsonString(TASK))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        get()
                .andExpect(jsonPath("$.id").value(TASK.getId()))
                .andExpect(jsonPath("$.name").value(TASK.getName()));
    }

    @Test
    @SneakyThrows
    public void put() {
        post();
        TASK.setName("TestPost");
        mockMvc.perform(
                MockMvcRequestBuilders.put("/api/task")
                        .content(asJsonString(TASK))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        get()
                .andExpect(jsonPath("$.id").value(TASK.getId()))
                .andExpect(jsonPath("$.name").value(TASK.getName()));
    }

    @Test
    @SneakyThrows
    public void delete() {
        post();
        mockMvc.perform(
                MockMvcRequestBuilders.delete("/api/task/" + TASK.getId())
                        .content(asJsonString(TASK))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        get()
                .andExpect(jsonPath("$").doesNotExist());
    }

    @NotNull
    @SneakyThrows
    private ResultActions get() {
        return mockMvc.perform(
                MockMvcRequestBuilders.get("/api/task/" + TASK.getId())
                        .content(asJsonString(TASK))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}
