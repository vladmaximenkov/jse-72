package ru.vmaksimenkov.tm;

import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.dto.ProjectRecord;
import ru.vmaksimenkov.tm.dto.TaskRecord;

import java.util.ArrayList;
import java.util.List;

public class TestData {

    @NotNull
    final protected static TaskRecord TASK = new TaskRecord("Task Test");

    @NotNull
    final protected static TaskRecord TASK2 = new TaskRecord("Task Test 2");

    @NotNull
    final protected static List<TaskRecord> TASK_LIST = new ArrayList<>();

    @NotNull
    final protected static ProjectRecord PROJECT = new ProjectRecord("Project Test");

    @NotNull
    final protected static ProjectRecord PROJECT2 = new ProjectRecord("Project Test 2");

    @NotNull
    final protected static List<ProjectRecord> PROJECT_LIST = new ArrayList<>();

    @NotNull
    protected static String USER_ID;

    {
        TASK_LIST.add(TASK);
        TASK_LIST.add(TASK2);
        PROJECT_LIST.add(PROJECT);
        PROJECT_LIST.add(PROJECT2);
    }

}
