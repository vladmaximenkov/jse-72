package ru.vmaksimenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.vmaksimenkov.tm.AbstractTest;
import ru.vmaksimenkov.tm.dto.TaskRecord;

import java.util.Objects;

public class TaskRecordServiceTest extends AbstractTest {

    @Autowired
    private TaskRecordService service;

    @Test
    public void clear() {
        service.removeByUserId(USER_ID);
        Assert.assertEquals(0, service.count(USER_ID));
    }

    @Test
    public void save() {
        clear();
        service.merge(USER_ID, TASK);
        Assert.assertEquals(1, service.count(USER_ID));
        Assert.assertEquals(read().getName(), TASK.getName());
    }

    @Test
    public void update() {
        TASK.setName("Test task name 2");
        service.merge(TASK);
        Assert.assertEquals("Test task name 2", read().getName());
    }

    @NotNull
    private TaskRecord read() {
        return Objects.requireNonNull(service.findByUserIdAndId(USER_ID, TASK.getId()));
    }

}
