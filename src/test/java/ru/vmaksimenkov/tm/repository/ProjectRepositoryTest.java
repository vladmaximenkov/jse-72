package ru.vmaksimenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import ru.vmaksimenkov.tm.AbstractTest;
import ru.vmaksimenkov.tm.dto.ProjectRecord;
import ru.vmaksimenkov.tm.repository.dto.ProjectRecordRepository;

public class ProjectRepositoryTest extends AbstractTest {

    @Autowired
    private ProjectRecordRepository repository;

    @NotNull
    private ProjectRecord read() {
        @Nullable final ProjectRecord projectRecord = repository.findByUserIdAndId(USER_ID, PROJECT.getId());
        Assert.assertNotNull(projectRecord);
        return projectRecord;
    }

    @Test
    public void clear() {
        repository.deleteAll();
        Assert.assertEquals(0, repository.count());
    }

    @Test
    public void save() {
        clear();
        repository.save(PROJECT);
        Assert.assertEquals(PROJECT.getName(), read().getName());
    }

    @Test
    public void update() {
        save();
        PROJECT.setName("New test project name");
        repository.save(PROJECT);
        Assert.assertEquals("New test project name", read().getName());
    }

}
