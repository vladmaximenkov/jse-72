package ru.vmaksimenkov.tm;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.vmaksimenkov.tm.config.DatabaseConfiguration;
import ru.vmaksimenkov.tm.config.WebApplicationConfiguration;
import ru.vmaksimenkov.tm.dto.AbstractBusinessEntityRecord;
import ru.vmaksimenkov.tm.model.CustomUser;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Objects;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class, DatabaseConfiguration.class})
public class AbstractTest extends TestData {

    {
        System.setProperty("org.apache.cxf.useSpringClassHelpers", "false");
    }

    @PersistenceContext
    protected EntityManager entityManager;

    @Autowired
    protected WebApplicationContext context;

    protected MockMvc mockMvc;

    @Autowired
    protected AuthenticationManager authenticationManager;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication =
                authenticationManager.authenticate(token);
        Assert.assertTrue(authentication.getPrincipal() instanceof CustomUser);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        USER_ID = Objects.requireNonNull(((CustomUser) authentication.getPrincipal()).getId());
        for (@NotNull final AbstractBusinessEntityRecord a: new AbstractBusinessEntityRecord[] { TASK, TASK2, PROJECT, PROJECT2 }) {
            a.setUserId(USER_ID);
        }

    }

    @NotNull
    protected static String asJsonString(@NotNull final Object obj) {
        try {
            @NotNull final ObjectMapper mapper = new ObjectMapper();
            @NotNull final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e);
        }
    }

}
