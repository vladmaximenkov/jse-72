package ru.vmaksimenkov.tm.controller;

import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.vmaksimenkov.tm.AbstractTest;
import ru.vmaksimenkov.tm.repository.dto.ProjectRecordRepository;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class ProjectControllerTest extends AbstractTest {

    @Autowired
    private ProjectRecordRepository repository;

    private String projectId;

    @Test
    @SneakyThrows
    public void create() {
        repository.deleteAll();
        mockMvc.perform(MockMvcRequestBuilders.get("/project/create"))
                .andDo(print())
                .andExpect(view().name("redirect:/projects"));
        Assert.assertEquals(1, repository.count());
    }

    @Test
    @SneakyThrows
    public void delete() {
        create();
        mockMvc.perform(MockMvcRequestBuilders.get("/project/delete/" + repository.findAll().get(0).getId()))
                .andExpect(view().name("redirect:/projects"));
        Assert.assertEquals(0, repository.count());
    }

    @Test
    @SneakyThrows
    public void getAll() {
        create();
        mockMvc.perform(MockMvcRequestBuilders.get("/projects"))
                .andExpect(status().isOk())
                .andExpect(view().name("project-list"));
    }

}
