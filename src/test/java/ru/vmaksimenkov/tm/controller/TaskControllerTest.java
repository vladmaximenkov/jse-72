package ru.vmaksimenkov.tm.controller;

import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.vmaksimenkov.tm.AbstractTest;
import ru.vmaksimenkov.tm.repository.dto.TaskRecordRepository;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class TaskControllerTest extends AbstractTest {

    @Autowired
    private TaskRecordRepository repository;

    private String taskId;

    @Test
    @SneakyThrows
    public void create() {
        repository.deleteAll();
        mockMvc.perform(MockMvcRequestBuilders.get("/task/create"))
                .andDo(print())
                .andExpect(view().name("redirect:/tasks"));
        Assert.assertEquals(1, repository.count());
    }

    @Test
    @SneakyThrows
    public void delete() {
        create();
        mockMvc.perform(MockMvcRequestBuilders.get("/task/delete/" + repository.findAll().get(0).getId()))
                .andExpect(view().name("redirect:/tasks"));
        Assert.assertEquals(0, repository.count());
    }

    @Test
    @SneakyThrows
    public void getAll() {
        create();
        mockMvc.perform(MockMvcRequestBuilders.get("/tasks"))
                .andExpect(status().isOk())
                .andExpect(view().name("task-list"));
    }

}
